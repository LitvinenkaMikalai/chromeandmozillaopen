package org.itstep.qa.firstautotest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeTest {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver75.exe");
        WebDriver chromDriver = new ChromeDriver();

        chromDriver.navigate().to("https://vwts.ru/");
        // установим полноэкранный размер окна
        chromDriver.manage().window().maximize();
    }
}
