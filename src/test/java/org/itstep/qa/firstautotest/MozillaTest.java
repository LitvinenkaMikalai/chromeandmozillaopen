package org.itstep.qa.firstautotest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MozillaTest {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver024.exe");
        WebDriver mozillaDriver = new FirefoxDriver();

        mozillaDriver.navigate().to("http://www.toyota-club.by/");
        // установим полноэкранный размер окна
        mozillaDriver.manage().window().maximize();
    }
}
